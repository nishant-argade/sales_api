<?php

namespace App\Http\Controllers;

use App\Models\PayoutStat;
use Illuminate\Http\Request;
use App\Models\OutletSale;
use App\Jobs\CheckHighPayoutOutlets;

class OrderController extends Controller
{
    public function checkHighPayoutOutlets()
    {
        CheckHighPayoutOutlets::dispatch();
        return response()->json(['message' => 'Job dispatched to check high payout outlets and store their info in a table.']);
    }

    public function getStatistics(Request $request)
    {

        // Validation
        $validated = $request->validate([
            'start_date' => 'required|date_format:Y-m-d',
            'end_date' => 'required|date_format:Y-m-d|after_or_equal:start_date',
            'outlets' => 'nullable|string',
            'brands' => 'nullable|string',
            'groups' => 'nullable|string',
            'platform' => 'nullable|string',
        ]);

        $startDate = $validated['start_date'];
        $endDate = $validated['end_date'];
        $outlets = $validated['outlets'] ?? null;
        $brands = $validated['brands'] ?? null;
        $groups = $validated['groups'] ?? null;
        $platforms = $validated['platform'] ?? null;

        $query = OutletSale::query();

        // Error Handling and Fetching Data
        try {

            if ($outlets) {
                $query->whereHas('outlet', function ($query) use ($outlets) {
                    $query->whereIn('id', explode(',', $outlets));
                });
            }

            if ($brands) {
                $query->whereHas('outlet.brand', function ($query) use ($brands) {
                    $query->whereIn('id', explode(',', $brands));
                });
            }

            if ($groups) {
                $query->whereHas('outlet.brand.group', function ($query) use ($groups) {
                    $query->whereIn('id', explode(',', $groups));
                });
            }

            if ($platforms) {
                $query->whereIn('platform_type', explode(',', $platforms));
            }

            $startDate = \Carbon\Carbon::parse($startDate);
            $endDate = \Carbon\Carbon::parse($endDate);

            $currentData = $this->getDataForPeriod($query, $startDate, $endDate);
            $previousData = $this->getDataForPeriod($query, $startDate->copy()->subMonth(), $endDate->copy()->subMonth());

            $dataByStartOfWeek = $this->getDataByStartOfWeek($query, $startDate, $endDate->addDay());


        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }

        // Calculate the total sales for each period
        $currTotalOrder = floatval($currentData['total_orders']);
        $currTotalSale = floatval($currentData['total_sale']);
        $currTotalDiscount = floatval($currentData['total_discount']);
        $currTotalAdSpend = floatval($currentData['ad_spends']);
        $currTotalPayout = floatval($currentData['payout']);


        $prevTotalOrder = floatval($previousData['total_orders']);
        $prevTotalSale = floatval($previousData['total_sale']);
        $prevTotalDiscount = floatval($previousData['total_discount']);
        $prevTotalAdSpend = floatval($previousData['ad_spends']);
        $prevTotalPayout = floatval($previousData['payout']);


        $percentageDeltaTotalOrder = $this->calculatePercentage($currTotalOrder, $prevTotalOrder);
        $percentageDeltaTotalSale = $this->calculatePercentage($currTotalSale, $prevTotalSale);
        $percentageDeltaTotalDiscount = $this->calculatePercentage($currTotalDiscount, $prevTotalDiscount);
        $percentageDeltaTotalAdSpend = $this->calculatePercentage($currTotalAdSpend, $prevTotalAdSpend);
        $percentageDeltaTotalPayout = $this->calculatePercentage($currTotalPayout, $prevTotalPayout);

        $total_payout_by_sow = round($dataByStartOfWeek['total_payout'], 2);
        $total_ad_spends_by_sow = round($dataByStartOfWeek['total_ad_spends'], 2);

        return response()->json([
            'data' => [
                'Total Orders' => $currTotalOrder,
                'Total sale' => $currTotalSale,
                'Total Discount' => $currTotalDiscount,
                'Total Ad spend' => $currTotalAdSpend,
                'Total Payout' => $currTotalPayout,

                'percentage_delta_orders' => $percentageDeltaTotalOrder,
                'percentage_delta_revenue' => $percentageDeltaTotalSale,
                'percentage_delta_discount' => $percentageDeltaTotalDiscount,
                'percentage_delta_ad_spend' => $percentageDeltaTotalAdSpend,
                'percentage_delta_payout' => $percentageDeltaTotalPayout,

                'total_payout_by_sow' => $total_payout_by_sow,
                'total_ad_spends_by_sow' => $total_ad_spends_by_sow,
            ],
        ]);
    }


    private function calculatePercentage($currValue, $prevValue)
    {
        return $prevValue != 0 ? round((($currValue - $prevValue) / abs($prevValue)) * 100, 2) : 0;
    }

    private function getDataForPeriod($query, $startDate, $endDate)
    {
        $queryCopy = clone $query;
        return $queryCopy->whereBetween('sale_date', [$startDate, $endDate])
            ->selectRaw('ROUND(SUM(total_orders),2) as total_orders, ROUND(SUM(total_sale),2) as total_sale, ROUND(SUM(total_discount),2) as total_discount, ROUND(SUM(ad_spends),2) as ad_spends, ROUND(SUM(payout),2) as payout')
            ->first();
    }

    private function getDataByStartOfWeek($query, $startDate, $endDate)
    {
        $queryCopy = clone $query;
        $sales = $queryCopy->whereBetween('sale_date', [$startDate, $endDate])
            ->orderBy('sale_date', 'asc')
            ->get();

        $totalPayout = 0;
        $extraDayPayout = 0;
        $lastWeekPayout = -1;
        $totalAdSpends = 0;
        $extraDayAdSpends = 0;
        $lastWeekAdSpends = -1;


        foreach ($sales as $index => $sale) {

            $isLastRecord = $index == count($sales) - 1;

            if ($sale->start_of_week) {

                $totalPayout += $lastWeekPayout == -1 ? $extraDayPayout : $lastWeekPayout;
                $totalAdSpends += $lastWeekAdSpends == -1 ? $extraDayAdSpends : $lastWeekAdSpends;

                $extraDayPayout = $isLastRecord ? 0 : $sale->payout_daily ?? 0;
                $lastWeekPayout = $sale->payout ?? 0;

                $extraDayAdSpends = $isLastRecord ? 0 : $sale->ad_spends_daily ?? 0;
                $lastWeekAdSpends = $sale->ad_spends ?? 0;

            } else {
                $extraDayPayout += $isLastRecord ? 0 : $sale->payout_daily ?? 0;
                $extraDayAdSpends += $isLastRecord ? 0 : $sale->ad_spends_daily ?? 0;
            }
        }

        $totalPayout += $extraDayPayout;
        $totalAdSpends += $extraDayAdSpends;


        return [
            'total_payout' => $totalPayout,
            'total_ad_spends' => $totalAdSpends,
        ];

    }
}
