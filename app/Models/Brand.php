<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Outlet;

class Brand extends Model
{
    public function outlets()
    {
        return $this->hasMany(Outlet::class);
    }
}
