<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use App\Models\Brand;
use App\Models\OutletSale;

class Outlet extends Model
{
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function sales()
    {
        return $this->hasMany(OutletSale::class);
    }
}
