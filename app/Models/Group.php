<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Brand;

class Group extends Model
{
    public function brands()
    {
        return $this->hasMany(Brand::class);
    }
}
