<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Outlet;

class OutletSale extends Model
{
    public function outlet()
    {
        return $this->belongsTo(Outlet::class);
    }
}
