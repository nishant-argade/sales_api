<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Outlet;

class PayoutStat extends Model
{
    protected $table = 'payout_stats';
    public $timestamps = false;

    protected $fillable = [
        'outlet_id',
        'month',
        'sale',
        'payout',
        'payout_pct'
    ];

    public function outlet()
    {
        return $this->belongsTo(Outlet::class);
    }
}
