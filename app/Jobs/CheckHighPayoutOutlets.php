<?php

namespace App\Jobs;

use App\Models\OutletSale;
use App\Models\PayoutStat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class CheckHighPayoutOutlets implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        $outletSales = OutletSale::all();

        foreach ($outletSales as $outletSale) {


            $sale = floatval($outletSale->total_sale);
            $payout = floatval($outletSale->payout);
            $payout_pct = $sale != 0 ? round(($payout / $sale) * 100, 2) : 0;
            $outlet_id = $outletSale->outlet_id;
            $month = \Carbon\Carbon::parse($outletSale->sale_date)->format('M');


            PayoutStat::create([
                'outlet_id' => $outlet_id,
                'month' => $month,
                'sale' => $sale,
                'payout' => $payout,
                'payout_pct' => $payout_pct
            ]);

        }

    }
}
