<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/sales', [OrderController::class, 'getStatistics']);
Route::get('/checkHighPayoutOutlets', [OrderController::class, 'checkHighPayoutOutlets']);